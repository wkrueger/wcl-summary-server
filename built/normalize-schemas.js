"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const normalizr_1 = require("normalizr");
const lodash_1 = require("lodash");
const mergeObjects = (a, b) => {
    if (typeof a !== "object" || typeof b !== "object")
        return a;
    return Object.assign({}, a, b);
};
exports.config = new normalizr_1.schema.Entity("configs");
exports.fight = new normalizr_1.schema.Entity("fights", {}, { mergeStrategy: mergeObjects });
exports.source = new normalizr_1.schema.Entity("sources", { fights: [exports.fight] });
exports.bossPhase = new normalizr_1.schema.Entity("bossPhases", {}, { idAttribute: "boss" });
exports.report = new normalizr_1.schema.Entity("reports", {
    enemies: [exports.source],
    enemyPets: [exports.source],
    fights: [exports.fight],
    friendlies: [exports.source],
    friendlyPets: [exports.source],
    phases: [exports.bossPhase]
}, { mergeStrategy: mergeObjects });
exports.spell = new normalizr_1.schema.Entity("spells", {}, {
    idAttribute: "guid",
    mergeStrategy: mergeObjects
});
const baseEvent = {
    config: exports.config,
    source: exports.source,
    target: exports.source
};
exports.childEvent = new normalizr_1.schema.Entity("childEvents", Object.assign({}, baseEvent, { ability: exports.spell }), {
    idAttribute: (value, parent, key) => {
        return (parent.timestamp +
            ":" +
            parent.source +
            ":" +
            lodash_1.get(value, "ability.guid") +
            ":" +
            value.timestamp);
    }
});
exports.deathEvent = new normalizr_1.schema.Entity("deathEvents", Object.assign({}, baseEvent, { childEvents: [exports.childEvent], killingBlow: exports.spell, report: exports.report, fight: exports.fight }), {
    idAttribute: value => value.timestamp + ":" + value.source.id
});
exports.mainResponse = new normalizr_1.schema.Entity("mainResponse", {
    report: exports.report,
    supportedFights: [exports.fight],
    deathEvents: [exports.deathEvent]
});
//# sourceMappingURL=normalize-schemas.js.map