"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cacheManager = require("cache-manager");
const fsStore = require("cache-manager-fs");
const diskCache = cacheManager.caching({
    store: fsStore,
    options: {
        ttl: 60 * 60,
        path: "../cache",
        preventfill: false,
        maxsize: 1000 * 1000 * 10 //bytes
    }
});
function cacheWrap(fn, cacheKeyGen = i => String(i)) {
    return async (i) => {
        const cacheKey = cacheKeyGen(i);
        let resp = await diskCache.wrap(cacheKey, () => fn(i));
        return resp;
    };
}
exports.cacheWrap = cacheWrap;
//# sourceMappingURL=cache.js.map