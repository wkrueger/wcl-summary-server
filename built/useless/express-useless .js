"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const yup = require("yup");
const ono = require("ono");
exports.schema = yup.object;
async function json(router, setup) {
    router.use(express.json());
    await setup(router, helpers);
    const errorMiddleware = (err, req, res, next) => {
        return res
            .status(err.statusCode || 500)
            .json({ error: { message: err.message, stack: err.stack } });
    };
    router.use(errorMiddleware);
}
exports.json = json;
const helpers = {
    withValidation(what, mw) {
        return async (req, res, next) => {
            try {
                if (what.query)
                    req.query = what.query.validateSync(req.query);
                if (what.params)
                    req.params = what.params.validateSync(req.params);
                await mw(req, res, next);
            }
            catch (err) {
                next(ono(err));
            }
        };
    }
};
//# sourceMappingURL=express-useless .js.map