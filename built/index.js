"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const LogParse = require("./logparse");
const express = require("express");
const express_useless_1 = require("./useless/express-useless ");
const cors = require("cors");
const yup = require("yup");
(async () => {
    const app = express();
    app.use(cors());
    await express_useless_1.json(app, async (app, h) => {
        app.get("/report", h.withValidation({
            query: yup.object({
                reportId: yup.string().required(),
                deathCut: yup.number().default(2)
            })
        }, async (req, res) => {
            const reportResp = await LogParse.analyzeReport(req.query.reportId.trim(), req.query.deathCut);
            console.log("successful response for id", req.query.reportId);
            res.send(reportResp);
        }));
    });
    const port = process.env.PORT || 8000;
    app.listen(port, () => console.log(port));
})();
process.on("unhandledRejection", err => {
    console.log(err);
    process.exit(1);
});
//# sourceMappingURL=index.js.map