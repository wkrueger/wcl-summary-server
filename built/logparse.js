"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WF = require("@wipefest/core");
const axios_1 = require("axios");
const dotEnv = require("dotenv");
const core_1 = require("@wipefest/core");
const normalizr_1 = require("normalizr");
const cache_1 = require("./useless/cache");
const Schemas = require("./normalize-schemas");
dotEnv.config({
    path: "../envfile.env"
});
const wclHttp = axios_1.default.create({
    baseURL: "https://www.warcraftlogs.com/v1/"
});
//debugging which requests are made
wclHttp.interceptors.request.use(function (config) {
    console.log("request", config.url);
    return config;
}, function (error) {
    return Promise.reject(error);
});
const evConfigHttp = axios_1.default.create({
    baseURL: "https://raw.githubusercontent.com/JoshYaxley/Wipefest.EventConfigs/master/"
});
const apiKey = process.env.WCL_PUBLIC;
var Svc;
(function (Svc) {
    Svc.report = new WF.WarcraftLogsReportService(wclHttp, apiKey);
    Svc.death = new WF.WarcraftLogsDeathService(wclHttp, apiKey);
    Svc.encounter = new WF.EncountersService();
    Svc.spec = new WF.SpecializationsService();
    Svc.evConfig = new WF.EventConfigService(Svc.encounter, Svc.spec, evConfigHttp);
    Svc.wclCombatEvent = new WF.WarcraftLogsCombatEventService(Svc.evConfig, wclHttp, apiKey);
    Svc.fightEvent = new WF.FightEventService();
    Svc.insight = new WF.InsightService();
    Svc.fight = new WF.FightService(Svc.death, Svc.wclCombatEvent, Svc.fightEvent, Svc.spec, Svc.insight);
})(Svc || (Svc = {}));
async function $obs(o) {
    const response = await o.toPromise();
    if (response.isFailure)
        throw Error(response.error);
    return response.value;
}
const cached = {
    preNormalize: cache_1.cacheWrap((i) => {
        return preNormalize(i.reportId.trim(), i.deaths || 2);
    }, i => "prenorm:" + i.reportId.trim() + ":" + (i.deaths || 2))
};
async function analyzeReport(reportId, deaths) {
    const response = await cached.preNormalize({ reportId, deaths });
    const normalized = normalizr_1.normalize(response, Schemas.mainResponse);
    return Object.assign({ reportId, deathCut: deaths }, normalized);
}
exports.analyzeReport = analyzeReport;
async function preNormalize(logId, deaths) {
    const report = await $obs(Svc.report.getReport(logId));
    const supportedFights = report.fights.filter(fight => Svc.encounter.getEncounters().some(encounter => encounter.id === fight.boss));
    const allDeaths = await Promise.all(supportedFights.map(async (fight) => (await eachFight(report, fight, deaths)).deaths));
    const flatDeathEvents = allDeaths.reduce((out, curr) => [...out, ...curr], []);
    //const tidyUpDeaths: DeathEventT[] = flatDeathEvents.map(death => _.omit(death, ["config"])) as any
    return {
        report,
        supportedFights,
        deathEvents: flatDeathEvents
    };
}
async function eachFight(report, fightInfo, deaths) {
    const includes = await $obs(Svc.evConfig.getIncludesForBoss(fightInfo.boss));
    const eventConfigs = await $obs(Svc.evConfig.getEventConfigs(["general/raid", ...includes]));
    const wipefest = await $obs(Svc.fight.getFight(report, fightInfo, eventConfigs));
    const cropped = cropEventsAfterNDeaths(wipefest.events, deaths);
    return cropped;
}
function cropEventsAfterNDeaths(events, limitDeaths) {
    let filtered = [];
    let deaths = [];
    for (let x = 0; x < events.length; x++) {
        const event = events[x];
        filtered.push(event);
        if (event instanceof core_1.FightEventModels.DeathEvent) {
            deaths.push(event);
            if (deaths.length >= limitDeaths)
                return { events: filtered, deaths };
        }
    }
    return { events: filtered, deaths };
}
//# sourceMappingURL=logparse.js.map