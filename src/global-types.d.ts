declare module "ono"
declare module "cache-manager"
declare module "cache-manager-fs"

type Omit<T, K extends keyof T> = Pick<
  T,
  ({ [P in keyof T]: P } &
    { [P in K]: never } & { [x: string]: never; [x: number]: never })[keyof T]
>
