import { schema } from "normalizr"
import { get as _get } from "lodash"

const mergeObjects = (a: any, b: any) => {
  if (typeof a !== "object" || typeof b !== "object") return a
  return { ...a, ...b }
}

export const config = new schema.Entity("configs")
export const fight = new schema.Entity("fights", {}, { mergeStrategy: mergeObjects })
export const source = new schema.Entity("sources", { fights: [fight] })
export const bossPhase = new schema.Entity("bossPhases", {}, { idAttribute: "boss" })

export const report = new schema.Entity(
  "reports",
  {
    enemies: [source],
    enemyPets: [source],
    fights: [fight],
    friendlies: [source],
    friendlyPets: [source],
    phases: [bossPhase]
  },
  { mergeStrategy: mergeObjects }
)

export const spell = new schema.Entity(
  "spells",
  {},
  {
    idAttribute: "guid",
    mergeStrategy: mergeObjects
  }
)
const baseEvent = {
  config: config,
  source: source,
  target: source
}
export const childEvent = new schema.Entity(
  "childEvents",
  {
    ...baseEvent,
    ability: spell
  },
  {
    idAttribute: (value, parent, key) => {
      return (
        parent.timestamp +
        ":" +
        parent.source +
        ":" +
        _get(value, "ability.guid") +
        ":" +
        value.timestamp
      )
    }
  }
)

export const deathEvent = new schema.Entity(
  "deathEvents",
  {
    ...baseEvent,
    childEvents: [childEvent],
    killingBlow: spell,
    report: report,
    fight: fight
  },
  {
    idAttribute: value => value.timestamp + ":" + value.source.id
  }
)

export const mainResponse = new schema.Entity("mainResponse", {
  report,
  supportedFights: [fight],
  deathEvents: [deathEvent]
})
