import LogParse = require("./logparse")
import * as express from "express"
import { json } from "./useless/express-useless "
import cors = require("cors")
import * as yup from "yup"
//import { cacheWrap } from "./useless/cache"

/*
const cached = {
  analyzeReport: cacheWrap(LogParse.analyzeReport, id => "report-" + id)
}
*/

// ---
;(async () => {
  const app = express()
  app.use(cors())
  await json(app, async (app, h) => {
    app.get(
      "/report",
      h.withValidation(
        {
          query: yup.object({
            reportId: yup.string().required(),
            deathCut: yup.number().default(2)
          })
        },
        async (req, res) => {
          const reportResp = await LogParse.analyzeReport(
            req.query.reportId.trim(),
            req.query.deathCut
          )
          console.log("successful response for id", req.query.reportId)
          res.send(reportResp)
        }
      )
    )
  })
  const port = process.env.PORT || 8000
  app.listen(port, () => console.log(port))
})()

process.on("unhandledRejection", err => {
  console.log(err)
  process.exit(1)
})
