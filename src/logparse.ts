import * as WF from "@wipefest/core"
import axios from "axios"
import * as dotEnv from "dotenv"
import { Observable } from "rxjs/Observable"
import { HttpResult, FightEventModels } from "@wipefest/core"
import { normalize } from "normalizr"
import { cacheWrap } from "./useless/cache"
import * as Schemas from "./normalize-schemas"

dotEnv.config({
  path: "../envfile.env"
})

const wclHttp = axios.create({
  baseURL: "https://www.warcraftlogs.com/v1/"
})
//debugging which requests are made
wclHttp.interceptors.request.use(
  function(config) {
    console.log("request", config.url)
    return config
  },
  function(error) {
    return Promise.reject(error)
  }
)

const evConfigHttp = axios.create({
  baseURL: "https://raw.githubusercontent.com/JoshYaxley/Wipefest.EventConfigs/master/"
})

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      WCL_PUBLIC: string
      WCL_PRIVATE: string
      WCL_USER: string
    }
  }
}

const apiKey = process.env.WCL_PUBLIC

namespace Svc {
  export const report = new WF.WarcraftLogsReportService(wclHttp, apiKey)
  export const death = new WF.WarcraftLogsDeathService(wclHttp, apiKey)
  export const encounter = new WF.EncountersService()
  export const spec = new WF.SpecializationsService()
  export const evConfig = new WF.EventConfigService(encounter, spec, evConfigHttp)
  export const wclCombatEvent = new WF.WarcraftLogsCombatEventService(evConfig, wclHttp, apiKey)
  export const fightEvent = new WF.FightEventService()
  export const insight = new WF.InsightService()
  export const fight = new WF.FightService(death, wclCombatEvent, fightEvent, spec, insight)
}

async function $obs<T>(o: Observable<HttpResult<T>>) {
  const response = await o.toPromise()
  if (response.isFailure) throw Error(response.error)
  return response.value
}

const cached = {
  preNormalize: cacheWrap((i: { reportId: string; deaths: number }) => {
    return preNormalize(i.reportId.trim(), i.deaths || 2)
  }, i => "prenorm:" + i.reportId.trim() + ":" + (i.deaths || 2))
}

export async function analyzeReport(reportId: string, deaths: number) {
  const response = await cached.preNormalize({ reportId, deaths })
  const normalized = normalize(response, Schemas.mainResponse)
  return {
    reportId,
    deathCut: deaths,
    ...normalized
  }
}

async function preNormalize(logId: string, deaths: number) {
  const report = await $obs(Svc.report.getReport(logId))
  const supportedFights = report.fights.filter(fight =>
    Svc.encounter.getEncounters().some(encounter => encounter.id === fight.boss)
  )
  const allDeaths = await Promise.all(
    supportedFights.map(async fight => (await eachFight(report, fight, deaths)).deaths)
  )
  const flatDeathEvents = allDeaths.reduce((out, curr) => [...out, ...curr], [] as DeathEventT[])
  //const tidyUpDeaths: DeathEventT[] = flatDeathEvents.map(death => _.omit(death, ["config"])) as any
  return {
    report,
    supportedFights,
    deathEvents: flatDeathEvents
  }
}

async function eachFight(report: WF.Report, fightInfo: WF.FightInfo, deaths: number) {
  const includes = await $obs(Svc.evConfig.getIncludesForBoss(fightInfo.boss))
  const eventConfigs = await $obs(Svc.evConfig.getEventConfigs(["general/raid", ...includes]))
  const wipefest = await $obs(Svc.fight.getFight(report, fightInfo, eventConfigs))
  const cropped = cropEventsAfterNDeaths(wipefest.events, deaths)
  return cropped
}

type Event = WF.Fight["events"][0]
type DeathEventT = InstanceType<(typeof FightEventModels)["DeathEvent"]>

function cropEventsAfterNDeaths(events: Event[], limitDeaths: number) {
  let filtered: Event[] = []
  let deaths: DeathEventT[] = []
  for (let x = 0; x < events.length; x++) {
    const event = events[x]
    filtered.push(event)
    if (event instanceof FightEventModels.DeathEvent) {
      deaths.push(event)
      if (deaths.length >= limitDeaths) return { events: filtered, deaths }
    }
  }
  return { events: filtered, deaths }
}
