import express = require("express")
import * as yup from "yup"
import ono = require("ono")

export const schema = yup.object

export async function json(
  router: express.Router,
  setup: (router: express.Router, h: typeof helpers) => Promise<void>
): Promise<void> {
  router.use(express.json())
  await setup(router, helpers)
  const errorMiddleware: express.ErrorRequestHandler = (err, req, res, next) => {
    return res
      .status(err.statusCode || 500)
      .json({ error: { message: err.message, stack: err.stack } })
  }
  router.use(errorMiddleware)
}

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

const helpers = {
  withValidation<Q, P>(
    what: { query?: yup.ObjectSchema<Q>; params?: yup.ObjectSchema<P> },
    mw: (
      req: Omit<express.Request, "query" | "params"> & { query: Q; params: P },
      res: express.Response,
      next: express.NextFunction
    ) => void
  ) {
    return async (req: any, res: any, next: any) => {
      try {
        if (what.query) req.query = what.query.validateSync(req.query)
        if (what.params) req.params = what.params.validateSync(req.params)
        await mw(req, res, next)
      } catch (err) {
        next(ono(err))
      }
    }
  }
}
