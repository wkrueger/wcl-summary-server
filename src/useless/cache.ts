import cacheManager = require("cache-manager")
import fsStore = require("cache-manager-fs")
const diskCache = cacheManager.caching({
  store: fsStore,
  options: {
    ttl: 60 * 60, //seconds
    path: "../cache",
    preventfill: false,
    maxsize: 1000 * 1000 * 10 //bytes
  }
})

export function cacheWrap<Param, Resp>(
  fn: (i: Param) => Promise<Resp>,
  cacheKeyGen: (i: Param) => string = i => String(i)
) {
  return async (i: Param) => {
    const cacheKey = cacheKeyGen(i)
    let resp = await diskCache.wrap(cacheKey, () => fn(i))
    return resp as Resp
  }
}
